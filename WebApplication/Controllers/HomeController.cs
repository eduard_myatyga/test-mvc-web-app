﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            Init();//init 
        }
        private ViewModel model = new ViewModel();
        public ActionResult Index()
        {
            
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Peoples()
        {
            try
            {
                var result = JsonConvert.SerializeObject(model, Formatting.Indented,
    new JsonSerializerSettings()
    {
        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
    });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private void Init()
        {
            var p1 = new Person() { Name = "Sam" };
            var p2 = new Person() { Name = "Vasiliy" };
            var p3 = new Person() { Name = "Olga" };
            var p4 = new Person() { Name = "Den" };
            //            var p6 = new Person() { Name = "o4ko" };
            //            var p7 = new Person() { Name = "gabon" };

            p1.Freinds.AddRange(new List<Person>() { p2, p3, p4 });
            p2.Freinds.AddRange(new List<Person>() { p4, p1, p3 });
            p3.Freinds.AddRange(new List<Person>() { p4, p1 });
            model = new ViewModel();
            model.Persons = new List<Person>() { p1, p2, p3, p4 };
        }
    }
}