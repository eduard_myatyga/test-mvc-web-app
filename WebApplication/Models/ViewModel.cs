﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class ViewModel
    {
        public List<Person> Persons { get; set; }
    }

    public class Person
    {
        public Person()
        {
            Freinds = new List<Person>();
        }
        public string Name { get; set; }
        public List<Person> Freinds { get; set; }
    }
}